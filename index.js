// console.log("Hello World!");

const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address1 = ["258 Washington Ave NW,"];
const address2 = ["California 90011"];
console.log(`I live at ${address1} ${address2}`);

const animal = {
	name: "salt water crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

console.log(`Lolong was a ${animal.name}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(`${number}`);	
})
const sum = numbers.reduce((partialSum, a) => partialSum + a, 0);
console.log(`${sum}`);

/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age  = age;
		this.breed = breed;
	}
}

const myDog = new Dog ();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund"

console.log(myDog);




